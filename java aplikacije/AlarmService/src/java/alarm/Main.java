package alarm;

import java.util.Date;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import javax.jms.Topic;

public class Main {

    @Resource(lookup = "AlarmTopic")
    static Topic topic;
    @Resource(lookup = "UserServiceTopic")
    static Topic userTopic;
    @Resource(lookup = "jms/__defaultConnectionFactory")
    static ConnectionFactory connectionFactory;

    private static final String ALARM_SERVICE = "Alarm";
    private static final String TIP_PROPERTY = "Tip";
    private static final String PERIOD_PROPERTY = "Period";
    private static final String SONG_PROPERTY = "Song";
    private static final String USER_PROPERTY = "User";
    private static final int HOUR = 3600000;
    private static final int MINUTE = 60000;

    public static void main(String[] args) {
        JMSContext context = connectionFactory.createContext();
        JMSProducer producer = context.createProducer();
        JMSConsumer consumer = context.createSharedDurableConsumer(topic, ALARM_SERVICE);
        Alarm alarm = new Alarm();

        while (true) {
            try {
                Message message = consumer.receive();
                short tip = message.getShortProperty(TIP_PROPERTY);
                if (message instanceof ObjectMessage) {
                    ObjectMessage objectMessage = (ObjectMessage) message;
                    switch (tip) {
                        case 0: { //basic alarm
                            Date date = (Date) objectMessage.getObject();
                            TextMessage producerMessage = context.createTextMessage();
                            producerMessage.setText(alarm.scheduleAlarm(date, message.getIntProperty(USER_PROPERTY)));
                            producer.send(userTopic, producerMessage);
                            break;
                        }
                        case 1: {//periodic alarm
                            Date date = (Date) objectMessage.getObject();
                            Long period = objectMessage.getLongProperty(PERIOD_PROPERTY);
                            TextMessage producerMessage = context.createTextMessage();
                            producerMessage.setText(alarm.schedulePeriodicAlarm(date, period, message.getIntProperty(USER_PROPERTY)));
                            producer.send(userTopic, producerMessage);
                            break;
                        }
                        case 2://get specific time 
                        {
                            TextMessage producerMessage = context.createTextMessage();
                            producerMessage.setText(alarm.getSpecificTimeForAlarm());
                            producer.send(userTopic, producerMessage);
                            break;
                        }
                        case 3://change song
                        {
                            TextMessage producerMessage = context.createTextMessage();
                            producerMessage.setText(alarm.changeSongForAlarm(message.getStringProperty(SONG_PROPERTY), message.getIntProperty(USER_PROPERTY)));
                            producer.send(userTopic, producerMessage);
                            break;
                        }
                        case 4://set specific time
                        {
                            int id = (int) objectMessage.getObject();
                            Date date = null;
                            switch (id) {
                                case 1:
                                    date = new Date(System.currentTimeMillis() + MINUTE);
                                    break;
                                case 2:
                                    date = new Date(System.currentTimeMillis() + HOUR);
                                    break;
                                case 3:
                                    date = new Date(System.currentTimeMillis() + MINUTE / 30);
                                    break;
                                case 4:
                                    date = new Date(System.currentTimeMillis() + HOUR * 24);
                                    break;

                            }
                            TextMessage producerMessage = context.createTextMessage();
                            producerMessage.setText(alarm.scheduleAlarm(date, message.getIntProperty(USER_PROPERTY)));
                            producer.send(userTopic, producerMessage);
                            break;
                        }
                        default:
                            break;
                    }
                }
            } catch (Exception ex) {
                try {
                    ex.printStackTrace();
                    TextMessage producerMessage = context.createTextMessage();
                    producerMessage.setText("ERROR!!!!!\n" + ex.getMessage());
                    producer.send(userTopic, producerMessage);
                } catch (JMSException ex1) {
                    ex1.printStackTrace();
                }
            }
        }
    }
}
