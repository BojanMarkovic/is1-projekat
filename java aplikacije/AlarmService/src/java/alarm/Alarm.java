package alarm;

import entiteti.User;
import java.io.File;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Alarm {

    private static final String AUDIO_PATH = "C:\\Users\\admin\\Desktop\\AlarmService\\";
    private static final String AUDIO_FORMAT = ".wav";
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("AlarmServicePU");
    private final EntityManager em = emf.createEntityManager();

    public String scheduleAlarm(Date date, int userId) {
        try {
            User user = em.find(User.class, userId);
            Timer timer = new Timer(true);
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    doAlarm(user);
                }
            }, date);
            System.out.println("Basic alarm schedule at " + date);
            return "Alarm uspesno navijen u " + date;
        } catch (Exception e) {
            e.printStackTrace();
            return "Doslo je do greske u Basic alarm funkciji " + e.getMessage();
        }
    }

    public String schedulePeriodicAlarm(Date date, long period, int userId) {
        try {
            User user = em.find(User.class, userId);
            Timer timer = new Timer(true);
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    doAlarm(user);
                }
            }, date, period);
            System.out.println("Periodic alarm schedule at " + date + " interval: " + period / 1000 + " seconds");
            return "Alarm uspesno navijen u " + date + " sa ponavljanjem: " + period / 1000 + " sekundi";
        } catch (Exception e) {
            e.printStackTrace();
            return "Doslo je do greske u periodic alarm funkciji " + e.getMessage();
        }
    }

    public String getSpecificTimeForAlarm() {
        System.out.println("Get specific time for Alarm ");
        return "1. in one minute\n2. in one hour\n3. in 2 seconds\n4. in one day\n";
    }

    public String changeSongForAlarm(String song, int userId) {
        try {
            User user = em.find(User.class, userId);
            System.out.println("Set new Song for alarm");
            entiteti.Alarm alarm = em.createQuery("SELECT a FROM Alarm a WHERE a.user.iduser = :idUser", entiteti.Alarm.class)
                    .setParameter("idUser", user.getIduser()).getSingleResult();
            alarm.setSong(song);
            em.getTransaction().begin();
            em.persist(alarm);
            em.getTransaction().commit();
            return "Zvuk alarma uspesno promenjen " + song;
        } catch (Exception e) {
            e.printStackTrace();
            return "Doslo je do greske u change alarm song funkciji " + e.getMessage();
        }
    }

    private void doAlarm(User user) {
        try {
            System.out.println("Alarm!!!" + user.getUsername());
            String s = em.createQuery("SELECT a FROM Alarm a WHERE a.user.iduser = :id", entiteti.Alarm.class)
                    .setParameter("id", user.getIduser()).getSingleResult().getSong();
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File(AUDIO_PATH + s + AUDIO_FORMAT));
            Clip clip = AudioSystem.getClip();
            clip.open(audioIn);
            clip.start();
            Thread.sleep(10000);
            clip.stop();
        } catch (Exception e) {
            System.out.println("greska pri pozivanju alarma");
            e.printStackTrace();
        }
    }
}
