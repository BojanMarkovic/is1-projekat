package rs.etf.bg.ac.rs.mb160113d.paramConverters;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;

@Provider
public class MyParamConverterProvider implements ParamConverterProvider {

    @Override
    public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
        if (rawType.getName().equals(Date.class.getName())) {
            return new ParamConverter<T>() {
                @Override
                public T fromString(String value) {
                    try {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
                        Date date = simpleDateFormat.parse(value.replaceAll("\\.", "-").replaceAll(" ", "-").replaceAll(":", "-"));
                        return rawType.cast(date);
                    } catch (ParseException ex) {
                        ex.printStackTrace();
                    }
                    return null;
                }

                @Override
                public String toString(T value) {
                    if (value != null) {
                        return value.toString();
                    }
                    return null;
                }
            };
        }
        return null;
    }

}
