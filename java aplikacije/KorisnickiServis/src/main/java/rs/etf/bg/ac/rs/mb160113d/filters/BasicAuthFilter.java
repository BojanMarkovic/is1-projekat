package rs.etf.bg.ac.rs.mb160113d.filters;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.StringTokenizer;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import rs.etf.bg.ac.rs.mb160113d.entiteti.User;

@Provider
public class BasicAuthFilter implements ContainerRequestFilter {

    @PersistenceContext(unitName = "my_persistence_unit")
    EntityManager em;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        List<String> authHeaderValues = requestContext.getHeaders().get("Authorization");

        if (authHeaderValues != null && !authHeaderValues.isEmpty()) {
            String authHeaderValue = authHeaderValues.get(0);
            String decodedAuthHeaderValue = new String(Base64.getDecoder().decode(authHeaderValue.replaceFirst("Basic ", "")), StandardCharsets.UTF_8);
            StringTokenizer stringTokenizer = new StringTokenizer(decodedAuthHeaderValue, ":");
            String username = stringTokenizer.nextToken();
            String password = stringTokenizer.nextToken();

            List<User> users = em.createNamedQuery("User.findByUsername", User.class).setParameter("username", username).getResultList();
            if (users == null || users.isEmpty() || !users.get(0).getPassword().equals(password)) {
                Response response = Response.status(Response.Status.UNAUTHORIZED).entity("nije validan username ili password").build();
                requestContext.abortWith(response);
                return;
            }
            return;

        }

        Response response = Response.status(Response.Status.UNAUTHORIZED).entity("Los username ili password").build();
        requestContext.abortWith(response);
    }

}
