package rs.etf.bg.ac.rs.mb160113d.alarm.endpoints;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Stateless
@Path("main")
public class mainEndpoint {

    @Resource(lookup = "UserServiceTopic")
    Topic userTopic;
    @Resource(lookup = "jms/__defaultConnectionFactory")
    ConnectionFactory connectionFactory;

    @GET
    @Path("test")
    public Response test() {
        return Response.ok().build();
    }
    
    @GET
    @Path("receive")
    public Response receive() {
        JMSContext context = connectionFactory.createContext();
        JMSConsumer consumer = context.createConsumer(userTopic);
        Message message = consumer.receive(1000);

        if (message instanceof TextMessage) {
            try {
                return Response.ok(((TextMessage) message).getText()).build();
            } catch (JMSException ex) {
                return Response.ok("jms greska u mainEndpointu").build();
            }
        }

        return Response.ok().build();
    }
}
