package rs.etf.bg.ac.rs.mb160113d.alarm.endpoints;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.ObjectMessage;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import rs.etf.bg.ac.rs.mb160113d.entiteti.User;

@Stateless
@Path("planner")
public class Planner {

    @Resource(lookup = "PlannerTopic")
    Topic topic;
    @Resource(lookup = "jms/__defaultConnectionFactory")
    ConnectionFactory connectionFactory;
    @PersistenceContext(unitName = "my_persistence_unit")
    EntityManager em;
    private static final String TIP_PROPERTY = "Tip";
    private static final String OBAVEZA_PROPERTY = "OPIS_LOKACIJA";
    private static final String ID_PROPERTY = "ID_Property";
    private static final String USER_PROPERTY = "User";
    private static final String TRAJANJE = "Trajanje";

    @POST
    @Path("napraviObavezu/{opis}/{lokacija}/{datum}/{trajanje}")
    public Response createObaveza(@PathParam("opis") String opis, @PathParam("lokacija") String lokacija, @PathParam("datum") Date date,
            @PathParam("trajanje") short trajanje, @Context HttpHeaders httpHeaders) {
        try {
            if (date == null) {
                return Response.ok("Datum nije validan").build();
            }
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            ObjectMessage producerMessage = context.createObjectMessage(date);
            producerMessage.setLongProperty(TRAJANJE, trajanje);
            producerMessage.setStringProperty(OBAVEZA_PROPERTY, opis + "#" + lokacija);
            producerMessage.setShortProperty(TIP_PROPERTY, (short) 0);
            producerMessage.setIntProperty(USER_PROPERTY, getUser(httpHeaders).getIduser());
            producer.send(topic, producerMessage);
            return Response.ok().build();
        } catch (JMSException ex) {
            ex.printStackTrace();
            return Response.ok("Dogodila se greska " + ex.getMessage()).build();
        }
    }

    @POST
    @Path("obrisiObavezu/{id}")
    public Response deleteObaveza(@PathParam("id") int id, @Context HttpHeaders httpHeaders) {
        try {
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            ObjectMessage producerMessage = context.createObjectMessage(id);
            producerMessage.setShortProperty(TIP_PROPERTY, (short) 1);
            producerMessage.setIntProperty(USER_PROPERTY, getUser(httpHeaders).getIduser());
            producer.send(topic, producerMessage);
            return Response.ok().build();
        } catch (JMSException ex) {
            ex.printStackTrace();
            return Response.ok("Dogodila se greska " + ex.getMessage()).build();
        }
    }

    @GET
    @Path("sveObaveze/get")
    public Response getAllObaveza(@Context HttpHeaders httpHeaders) {
        try {
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            ObjectMessage producerMessage = context.createObjectMessage();
            producerMessage.setShortProperty(TIP_PROPERTY, (short) 2);
            producerMessage.setIntProperty(USER_PROPERTY, getUser(httpHeaders).getIduser());
            producer.send(topic, producerMessage);
            return Response.ok().build();
        } catch (JMSException ex) {
            ex.printStackTrace();
            return Response.ok("Dogodila se greska " + ex.getMessage()).build();
        }
    }

    @POST
    @Path("promeniObavezu/{id}/{opis}/{lokacija}/{datum}/{trajanje}")
    public Response modifyObaveza(@PathParam("id") int id, @PathParam("opis") String opis, @PathParam("lokacija") String lokacija, @PathParam("datum") Date date,
            @PathParam("trajanje") short trajanje, @Context HttpHeaders httpHeaders) {
        try {
            if (date == null) {
                return Response.ok("Datum nije validan").build();
            }
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            ObjectMessage producerMessage = context.createObjectMessage(date);
            producerMessage.setLongProperty(TRAJANJE, trajanje);
            producerMessage.setStringProperty(OBAVEZA_PROPERTY, id + "#" + opis + "#" + lokacija);
            producerMessage.setShortProperty(TIP_PROPERTY, (short) 3);
            producerMessage.setIntProperty(USER_PROPERTY, getUser(httpHeaders).getIduser());
            producer.send(topic, producerMessage);
            return Response.ok().build();
        } catch (JMSException ex) {
            ex.printStackTrace();
            return Response.ok("Dogodila se greska " + ex.getMessage()).build();
        }
    }

    @POST
    @Path("dodajAlarm/{id}")
    public Response dodajAlarm(@PathParam("id") int id, @Context HttpHeaders httpHeaders) {
        try {
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            ObjectMessage producerMessage = context.createObjectMessage();
            producerMessage.setShortProperty(TIP_PROPERTY, (short) 4);
            producerMessage.setLongProperty(ID_PROPERTY, (short) id);
            producerMessage.setIntProperty(USER_PROPERTY, getUser(httpHeaders).getIduser());
            producer.send(topic, producerMessage);
            return Response.ok().build();
        } catch (JMSException ex) {
            ex.printStackTrace();
            return Response.ok("Dogodila se greska " + ex.getMessage()).build();
        }
    }

    private User getUser(HttpHeaders httpHeaders) {
        List<String> authHeaderValues = httpHeaders.getRequestHeader("Authorization");

        User user = null;
        if (authHeaderValues != null && authHeaderValues.size() > 0) {
            String authHeaderValue = authHeaderValues.get(0);
            String decodedAuthHeaderValue = new String(Base64.getDecoder().decode(authHeaderValue.replaceFirst("Basic ", "")), StandardCharsets.UTF_8);
            StringTokenizer stringTokenizer = new StringTokenizer(decodedAuthHeaderValue, ":");
            String username = stringTokenizer.nextToken();

            user = em.createNamedQuery("User.findByUsername", User.class).setParameter("username", username).getSingleResult();
        }
        return user;
    }
}
