package rs.etf.bg.ac.rs.mb160113d.alarm.endpoints;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.StringTokenizer;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import rs.etf.bg.ac.rs.mb160113d.entiteti.User;

@Stateless
@Path("player")
public class Player {

    @Resource(lookup = "PlayerTopic")
    Topic topic;
    @Resource(lookup = "jms/__defaultConnectionFactory")
    ConnectionFactory connectionFactory;
    @PersistenceContext(unitName = "my_persistence_unit")
    EntityManager em;
    private static final String TIP_PROPERTY = "Tip";
    private static final String SONG_PROPERTY = "Song";
    private static final String USER_PROPERTY = "User";

    @POST
    @Path("pustiPesmu/{pesma}")
    public Response pustiPesmu(@PathParam("pesma") String pesma, @Context HttpHeaders httpHeaders) {
        try {
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            Message producerMessage = context.createMessage();
            producerMessage.setStringProperty(SONG_PROPERTY, pesma);
            producerMessage.setIntProperty(USER_PROPERTY, getUser(httpHeaders).getIduser());
            producerMessage.setShortProperty(TIP_PROPERTY, (short) 0);
            producer.send(topic, producerMessage);
            return Response.ok().build();
        } catch (JMSException ex) {
            ex.printStackTrace();
            return Response.ok("Dogodila se greska " + ex.getMessage()).build();
        }
    }

    @GET
    @Path("vratiIstoriju")
    public Response vratiIstoriju(@Context HttpHeaders httpHeaders) {
        try {
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            Message producerMessage = context.createMessage();
            producerMessage.setShortProperty(TIP_PROPERTY, (short) 1);
            producerMessage.setIntProperty(USER_PROPERTY, getUser(httpHeaders).getIduser());
            producer.send(topic, producerMessage);
            return Response.ok().build();
        } catch (JMSException ex) {
            ex.printStackTrace();
            return Response.ok("Dogodila se greska " + ex.getMessage()).build();
        }
    }

    private User getUser(HttpHeaders httpHeaders) {
        List<String> authHeaderValues = httpHeaders.getRequestHeader("Authorization");

        User user = null;
        if (authHeaderValues != null && authHeaderValues.size() > 0) {
            String authHeaderValue = authHeaderValues.get(0);
            String decodedAuthHeaderValue = new String(Base64.getDecoder().decode(authHeaderValue.replaceFirst("Basic ", "")), StandardCharsets.UTF_8);
            StringTokenizer stringTokenizer = new StringTokenizer(decodedAuthHeaderValue, ":");
            String username = stringTokenizer.nextToken();

            user = em.createNamedQuery("User.findByUsername", User.class).setParameter("username", username).getSingleResult();
        }
        return user;
    }
}
