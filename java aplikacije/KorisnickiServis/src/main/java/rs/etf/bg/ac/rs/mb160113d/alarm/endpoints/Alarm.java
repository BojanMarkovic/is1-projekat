package rs.etf.bg.ac.rs.mb160113d.alarm.endpoints;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.ObjectMessage;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import rs.etf.bg.ac.rs.mb160113d.entiteti.User;

@Stateless
@Path("alarm")
public class Alarm {

    @Resource(lookup = "AlarmTopic")
    Topic topic;
    @Resource(lookup = "jms/__defaultConnectionFactory")
    ConnectionFactory connectionFactory;
    @PersistenceContext(unitName = "my_persistence_unit")
    EntityManager em;

    private static final String TIP_PROPERTY = "Tip";
    private static final String PERIOD_PROPERTY = "Period";
    private static final String SONG_PROPERTY = "Song";
    private static final String USER_PROPERTY = "User";

    @POST
    @Path("basicAlarm/{datum}")
    public Response createBasicTimer(@PathParam("datum") Date date, @Context HttpHeaders httpHeaders) {
        try {
            if (date == null) {
                return Response.ok("Datum nije validan").build();
            }
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            ObjectMessage producerMessage = context.createObjectMessage(date);
            producerMessage.setShortProperty(TIP_PROPERTY, (short) 0);
            producerMessage.setIntProperty(USER_PROPERTY, getUser(httpHeaders).getIduser());
            producer.send(topic, producerMessage);
            return Response.ok().build();
        } catch (JMSException ex) {
            ex.printStackTrace();
            return Response.ok("Dogodila se greska " + ex.getMessage()).build();
        }
    }

    @POST
    @Path("periodicAlarm/{firstDatum}/{interval}")
    public Response createPeriodicTimer(@PathParam("firstDatum") Date date, @PathParam("interval") long interval, @Context HttpHeaders httpHeaders) {
        try {
            if (date == null) {
                return Response.ok("Datum nije validan").build();
            }
            if (interval < 0) {
                return Response.ok("interval ne sme biti negativan").build();
            }
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            ObjectMessage producerMessage = context.createObjectMessage(date);
            producerMessage.setShortProperty(TIP_PROPERTY, (short) 1);
            producerMessage.setLongProperty(PERIOD_PROPERTY, interval);
            producerMessage.setIntProperty(USER_PROPERTY, getUser(httpHeaders).getIduser());
            producer.send(topic, producerMessage);
            return Response.ok().build();
        } catch (JMSException ex) {
            ex.printStackTrace();
            return Response.ok("Dogodila se greska " + ex.getMessage()).build();
        }
    }

    @GET
    @Path("selectionAlarm/get")
    public Response getSelectionTimer() {
        try {
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            ObjectMessage producerMessage = context.createObjectMessage();
            producerMessage.setShortProperty(TIP_PROPERTY, (short) 2);
            producer.send(topic, producerMessage);
            return Response.ok().build();
        } catch (JMSException ex) {
            ex.printStackTrace();
            return Response.ok("Dogodila se greska " + ex.getMessage()).build();
        }
    }

    @POST
    @Path("selectionAlarm/{selectionId}")
    public Response createSelectionTimer(@PathParam("selectionId") int id, @Context HttpHeaders httpHeaders) {
        try {
            if (id < 0) {
                return Response.ok("Selektovani id ne sme biti negativan").build();
            }
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            ObjectMessage producerMessage = context.createObjectMessage(id);
            producerMessage.setShortProperty(TIP_PROPERTY, (short) 4);
            producerMessage.setIntProperty(USER_PROPERTY, getUser(httpHeaders).getIduser());
            producer.send(topic, producerMessage);
            return Response.ok().build();
        } catch (JMSException ex) {
            ex.printStackTrace();
            return Response.ok("Dogodila se greska " + ex.getMessage()).build();
        }
    }

    @POST
    @Path("changeMusic/{song}")
    public Response changeMusicAlarm(@PathParam("song") String song, @Context HttpHeaders httpHeaders) {
        try {
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            ObjectMessage producerMessage = context.createObjectMessage();
            producerMessage.setStringProperty(SONG_PROPERTY, song);
            producerMessage.setShortProperty(TIP_PROPERTY, (short) 3);
            producerMessage.setIntProperty(USER_PROPERTY, getUser(httpHeaders).getIduser());
            producer.send(topic, producerMessage);

            return Response.ok().build();
        } catch (JMSException ex) {
            ex.printStackTrace();
            return Response.ok("Dogodila se greska " + ex.getMessage()).build();
        }
    }

    private User getUser(HttpHeaders httpHeaders) {
        List<String> authHeaderValues = httpHeaders.getRequestHeader("Authorization");
        User user = null;
        if (authHeaderValues != null && authHeaderValues.size() > 0) {
            String authHeaderValue = authHeaderValues.get(0);
            String decodedAuthHeaderValue = new String(Base64.getDecoder().decode(authHeaderValue.replaceFirst("Basic ", "")), StandardCharsets.UTF_8);
            StringTokenizer stringTokenizer = new StringTokenizer(decodedAuthHeaderValue, ":");
            String username = stringTokenizer.nextToken();
            user = em.createNamedQuery("User.findByUsername", User.class).setParameter("username", username).getSingleResult();
        }
        return user;
    }
}
