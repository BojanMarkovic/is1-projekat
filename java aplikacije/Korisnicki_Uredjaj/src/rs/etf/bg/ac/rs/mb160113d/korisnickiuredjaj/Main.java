package rs.etf.bg.ac.rs.mb160113d.korisnickiuredjaj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Base64;
import java.util.Scanner;

public class Main {

    private static final String HOST = "http://localhost:8080/KorisnickiServis/api/";
    private static final String GET = "GET";
    private static final String POST = "POST";
    private static final String VALIDCHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._+";
    private static String username;
    private static String password;
    private static String mojaLokacija;

    private static boolean testIfValid(String test) {
        for (int i = 0; i < test.length(); i++) {
            if (!VALIDCHAR.contains(String.valueOf(test.charAt(i)))) {
                return false;
            }
        }
        return true;
    }

    private static boolean callGetMethod(String url) {
        try {
            URL u = new URL(url);
            String encoding = Base64.getEncoder().encodeToString((username + ":" + password).getBytes("UTF-8"));
            HttpURLConnection connection = (HttpURLConnection) u.openConnection();
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            connection.setRequestMethod(GET);
            connection.connect();
            int responseCode = connection.getResponseCode();
            switch (responseCode) {
                case HttpURLConnection.HTTP_OK:
                    try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                    String readLine;
                    while ((readLine = in.readLine()) != null) {
                        System.out.println(readLine);
                    }
                }
                return true;
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    System.out.println("Invalid user, you cant access this page" + "press any key to continue");
                    break;
                default:
                    System.out.println("GET NOT WORKED" + "press any key to continue");
                    break;
            }
        } catch (MalformedURLException ex) {
            System.out.println("URL not valid" + "press any key to continue");
            ex.printStackTrace();
        } catch (ProtocolException ex) {
            System.out.println("Protocol exception" + "press any key to continue");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Cant start a connection" + "press any key to continue");
            ex.printStackTrace();
        }
        return false;
    }

    private static void callPostMethod(String url) {
        try {
            URL u = new URL(url);
            String encoding = Base64.getEncoder().encodeToString((username + ":" + password).getBytes("UTF-8"));
            HttpURLConnection connection = (HttpURLConnection) u.openConnection();
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            connection.setRequestMethod(POST);

            connection.setDoOutput(true);
            connection.connect();
            int responseCode = connection.getResponseCode();
            switch (responseCode) {
                case HttpURLConnection.HTTP_OK:
                    try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                    String readLine;
                    while ((readLine = in.readLine()) != null) {
                        System.out.println(readLine);
                    }
                }
                break;
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    System.out.println("Invalid user, you cant access this page" + "press any key to continue");
                    break;
                default:
                    System.out.println("POST NOT WORKED" + "press any key to continue");
                    break;
            }
        } catch (MalformedURLException ex) {
            System.out.println("URL not valid" + "press any key to continue");
            ex.printStackTrace();
        } catch (ProtocolException ex) {
            System.out.println("Protocol exception" + "press any key to continue");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Cant start a connection" + "press any key to continue");
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        mojaLokacija = "Beograd";
        Scanner scanner = new Scanner(System.in);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        boolean flag = false;
        while (!flag) {
            System.out.println("Dobro dosli u pametnu kucu");
            System.out.println("Ulogujte se:");
            System.out.println("Korisnicko ime:");
            username = scanner.nextLine();
            System.out.println("Sifra:");
            password = scanner.nextLine();
            flag = callGetMethod("http://localhost:8080/KorisnickiServis/api/main/test");
        }
        while (true) {
            try {
                System.out.println("Izaberite funkcionalnost koju zelite (broj)");
                System.out.println("1. Reprodukcija zvuka\n2. Alarm\n3. Planer");
                int selectedSection = scanner.nextInt();
                int selectedOption;
                switch (selectedSection) {
                    case 1:
                        System.out.println("Music player");
                        System.out.println("Izaberite funkcionalnost koju zelite (broj)");
                        System.out.println("1. Pusti pesmu\n2. Prikazi moju istoriju");
                        System.out.println("0 za vracanje na prethodni meni");
                        selectedOption = scanner.nextInt();
                        switch (selectedOption) {
                            case 1:
                                System.out.println("Unesi ime pesme koju hocete da pustite");
                                String pesma = reader.readLine().replace(" ", "+");
                                if (!testIfValid(pesma)) {
                                    System.out.println("Input nije validan");
                                    break;
                                }
                                restCallAndWait(HOST + "player/pustiPesmu/" + pesma.replace(" ", username), false);
                                break;
                            case 2:
                                System.out.println("Vasa istorija slusanja je:");
                                restCallAndWait(HOST + "player/vratiIstoriju", true);
                                break;
                            case 0:
                                break;
                        }
                        break;
                    case 2:
                        System.out.println("Alarm");
                        System.out.println("Izaberite funkcionalnost koju zelite (broj)");
                        System.out.println("1. Navij obican alarm\n2. Navij alarm da se ponavlja\n3. Izaberi termin za alarm\n4. Promeni zvono alarma");
                        System.out.println("0 za vracanje na prethodni meni");
                        selectedOption = scanner.nextInt();
                        switch (selectedOption) {
                            case 1:
                                System.out.println("Napisite kog datuma zelite da navijete alarm u formatu DD.MM.GGGG");
                                String datum = scanner.next();
                                System.out.println("Napisite u koliko sati zelite da navijete alarm u formatu HH:MM:SS");
                                String vreme = scanner.next();
                                restCallAndWait(HOST + "alarm/basicAlarm/" + datum + "-" + vreme, false);
                                break;
                            case 2:
                                System.out.println("Napisite kog datuma zelite da navijete prvi alarm u formatu DD.MM.GGGG");
                                String datumPrvi = scanner.next();
                                System.out.println("Napisite u koliko sati zelite da navijete prvi alarm u formatu HH:MM:SS");
                                String vremePrvi = scanner.next();
                                System.out.println("Napisite koliko vremena kasnije zelite da bude ponavljanje alarma, u sekundama");
                                Long interval = scanner.nextLong() * 1000;
                                restCallAndWait(HOST + "alarm/periodicAlarm/" + datumPrvi + "-" + vremePrvi + "/" + interval, false);
                                break;
                            case 3:
                                System.out.println("Ponudjene mogucnosti za alarm su: - izaberite neku od njih (broj)");
                                restCallAndWait(HOST + "alarm/selectionAlarm/get", true);
                                int mogucnostAlarma = scanner.nextInt();
                                restCallAndWait(HOST + "alarm/selectionAlarm/" + mogucnostAlarma, false);
                                break;
                            case 4:
                                System.out.println("Unesite pesmu koju zelite da bude zvuk alarma");
                                String zvonoAlarma = reader.readLine().replace(" ", "+");
                                if (!testIfValid(zvonoAlarma)) {
                                    System.out.println("Input nije validan");
                                    break;
                                }
                                restCallAndWait(HOST + "alarm/changeMusic/" + zvonoAlarma, false);
                                break;
                            case 0:
                                break;
                        }
                        break;
                    case 3:
                        System.out.println("Planer");
                        System.out.println("Izaberite funkcionalnost koju zelite (broj)");
                        System.out.println("1. Napravi novu obavezu\n2. Obrisi obavezu\n3. Dohvati sve obaveze\n4. Izmeni obaveze\n5. Dodaj alarm obavezi");
                        System.out.println("0 za vracanje na prethodni meni");
                        selectedOption = scanner.nextInt();
                        switch (selectedOption) {
                            case 1: {
                                System.out.println("Unesite opis obaveze");
                                String opis = reader.readLine().replace(" ", "+");
                                if (!testIfValid(opis)) {
                                    System.out.println("Input nije validan");
                                    break;
                                }
                                System.out.println("Da li zelite da unesete lokaciju? 0- no, 1-yes");
                                int opcijaLokacije = scanner.nextInt();
                                String lokacija = "";
                                if (opcijaLokacije == 1) {
                                    System.out.println("Unesite lokaciju obaveze");
                                    lokacija = reader.readLine().replace(" ", "+");
                                    if (!testIfValid(lokacija)) {
                                        System.out.println("Input nije validan");
                                        break;
                                    }
                                }
                                System.out.println("Unesite datum obaveze u formatu DD.MM.GGGG");
                                String datum = scanner.next();
                                System.out.println("unesite sate u formatu HH:MM:SS");
                                String vreme = scanner.next();
                                System.out.println("Unesite trajanje obaveze u sekundama");
                                Short vremeTrajanje = scanner.nextShort();
                                if (opcijaLokacije == 1) {
                                    restCallAndWait(HOST + "planner/napraviObavezu/" + opis + "/" + lokacija + "/" + datum + "-" + vreme + "/" + vremeTrajanje, false);
                                } else {
                                    restCallAndWait(HOST + "planner/napraviObavezu/" + opis + "/" + mojaLokacija + "/" + datum + "-" + vreme + "/" + vremeTrajanje, false);
                                }
                                break;
                            }
                            case 2: {
                                System.out.println("Sve obaveze su: - izabrati onu koju zelite obrisati (-1 vraca na glavni meni)");
                                restCallAndWait(HOST + "planner/sveObaveze/get", true);
                                int obaveza = scanner.nextInt();
                                if (obaveza == -1) {
                                    break;
                                }
                                restCallAndWait(HOST + "planner/obrisiObavezu/" + obaveza, false);
                                break;
                            }
                            case 3:
                                System.out.println("Sve obaveze su:");
                                restCallAndWait(HOST + "planner/sveObaveze/get", true);
                                break;
                            case 4: {
                                System.out.println("Sve obaveze su: - izabrati onu koju zelite menjati (-1 vraca na glavni meni)");
                                restCallAndWait(HOST + "planner/sveObaveze/get", true);
                                int obaveza = scanner.nextInt();
                                if (obaveza == -1) {
                                    break;
                                }
                                System.out.println("Unesite nove vrednosti sa izabranu obavezu:");
                                System.out.println("Unesite opis obaveze");
                                String opis = reader.readLine().replace(" ", "+");
                                if (!testIfValid(opis)) {
                                    System.out.println("Input nije validan");
                                    break;
                                }
                                System.out.println("Da li zelite da unesete lokaciju? 0- no, 1-yes");
                                int opcijaLokacije = scanner.nextInt();
                                String lokacija = "";
                                if (opcijaLokacije == 1) {
                                    System.out.println("Unesite lokaciju obaveze");
                                    lokacija = reader.readLine().replace(" ", "+");
                                    if (!testIfValid(lokacija)) {
                                        System.out.println("Input nije validan");
                                        break;
                                    }
                                }
                                System.out.println("Unesite datum obaveze u formatu DD.MM.GGGG");
                                String datum = scanner.next();
                                System.out.println("unesite sate u formatu HH:MM:SS");
                                String vreme = scanner.next();
                                System.out.println("Unesite trajanje obaveze u sekundama");
                                Short vremeTrajanje = scanner.nextShort();
                                if (opcijaLokacije == 1) {
                                    restCallAndWait(HOST + "planner/promeniObavezu/" + obaveza + "/" + opis + "/" + lokacija + "/" + datum + "-" + vreme + "/" + vremeTrajanje, false);
                                } else {
                                    restCallAndWait(HOST + "planner/promeniObavezu/" + obaveza + "/" + opis + "/" + mojaLokacija + "/" + datum + "-" + vreme + "/" + vremeTrajanje, false);
                                }
                                break;
                            }
                            case 5:
                                System.out.println("Sve obaveze su: - izabrati onu kojoj zelite dodati alarm (-1 vraca na glavni meni)");
                                restCallAndWait(HOST + "planner/sveObaveze/get", true);
                                int obaveza = scanner.nextInt();
                                if (obaveza == -1) {
                                    break;
                                }
                                restCallAndWait(HOST + "planner/dodajAlarm/" + obaveza, false);
                                break;
                            case 0:
                                break;
                        }
                        break;
                }
            } catch (Exception e) {
                System.out.println("Greska " + e.getMessage());
            }
        }
    }

    private static void restCallAndWait(String url, boolean ifGetMethod) {
        Thread thread = new Thread(() -> {
            callGetMethod(HOST + "main/receive");
        });
        Thread thread2 = new Thread(() -> {
            if (ifGetMethod) {
                callGetMethod(url);
            } else {
                callPostMethod(url);
            }
        });
        thread.start();
        thread2.start();
        while (thread.isAlive() || thread2.isAlive());
    }
}
