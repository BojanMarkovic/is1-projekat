package player;

import entiteti.User;
import java.awt.Desktop;
import java.net.URI;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Player {

    private static final String YOUTUBE_URL = "https://www.youtube.com/embed?listType=search&list=";
    private static final String AUTOPLAY = "?&autoplay=1";
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("PlayerPU");
    private final EntityManager em = emf.createEntityManager();

    public String playSong(String song, int userId) {
        try {
            Desktop.getDesktop().browse(new URI(YOUTUBE_URL + song + AUTOPLAY));
            entiteti.Player player = new entiteti.Player();
            player.setSong(song.replace("+", " "));
            player.setUser(em.find(User.class, userId));
            em.getTransaction().begin();
            em.persist(player);
            em.getTransaction().commit();
            System.out.println("pusti pesmu " + song);
            return "Pusti pesmu " + song.replace("+", " ");
        } catch (Exception ex) {
            ex.printStackTrace();
            return "Doslo je do greske u pustanju pesme " + ex.getMessage();
        }

    }

    public String getHistory(int userId) {
        try {
            List<entiteti.Player> playeri = em.createQuery("SELECT p FROM Player p WHERE p.user.iduser = :idUser", entiteti.Player.class)
                    .setParameter("idUser", em.find(User.class, userId).getIduser()).getResultList();
            StringBuilder stringBuilder = new StringBuilder();
            System.out.println("vrati istoriju");
            for (entiteti.Player p : playeri) {
                stringBuilder.append(p.getSong()).append("\n");
            }
            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "Doslo je do greske u prikazivanju istorije slusanja " + e.getMessage();
        }
    }

}
