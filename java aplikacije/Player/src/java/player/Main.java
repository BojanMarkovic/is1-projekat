package player;

import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.Topic;

public class Main {

    @Resource(lookup = "PlayerTopic")
    static Topic topic;
    @Resource(lookup = "UserServiceTopic")
    static Topic userTopic;
    @Resource(lookup = "jms/__defaultConnectionFactory")
    static ConnectionFactory connectionFactory;

    private static final String PLAYER_SERVICE = "Player";
    private static final String TIP_PROPERTY = "Tip";
    private static final String SONG_PROPERTY = "Song";
    private static final String USER_PROPERTY = "User";

    public static void main(String[] args) {
        JMSContext context = connectionFactory.createContext();
        JMSProducer producer = context.createProducer();
        JMSConsumer consumer = context.createSharedDurableConsumer(topic, PLAYER_SERVICE);
        Player player = new Player();

        while (true) {
            try {
                Message message = consumer.receive();
                short tip = message.getShortProperty(TIP_PROPERTY);
                if (tip == 0) {//play song
                    TextMessage producerMessage = context.createTextMessage();
                    producerMessage.setText(player.playSong(message.getStringProperty(SONG_PROPERTY), message.getIntProperty(USER_PROPERTY)));
                    producer.send(userTopic, producerMessage);
                } else if (tip == 1) {//get history
                    TextMessage producerMessage = context.createTextMessage();
                    producerMessage.setText(player.getHistory(message.getIntProperty(USER_PROPERTY)));
                    producer.send(userTopic, producerMessage);
                }
            } catch (Exception ex) {
                try {
                    ex.printStackTrace();
                    TextMessage producerMessage = context.createTextMessage();
                    producerMessage.setText("ERROR!!!!!\n" + ex.getMessage());
                    producer.send(userTopic, producerMessage);
                } catch (JMSException ex1) {
                    ex1.printStackTrace();
                }
            }
        }
    }

}
