package entiteti;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "player")
@NamedQueries({
    @NamedQuery(name = "Player.findAll", query = "SELECT p FROM Player p"),
    @NamedQuery(name = "Player.findByIdplayer", query = "SELECT p FROM Player p WHERE p.idplayer = :idplayer"),
    @NamedQuery(name = "Player.findBySong", query = "SELECT p FROM Player p WHERE p.song = :song")})
public class Player implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idplayer")
    private Integer idplayer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "song")
    private String song;
    @JoinColumn(name = "user", referencedColumnName = "iduser")
    @ManyToOne(optional = false)
    private User user;

    public Player() {
    }

    public Player(Integer idplayer) {
        this.idplayer = idplayer;
    }

    public Player(Integer idplayer, String song) {
        this.idplayer = idplayer;
        this.song = song;
    }

    public Integer getIdplayer() {
        return idplayer;
    }

    public void setIdplayer(Integer idplayer) {
        this.idplayer = idplayer;
    }

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idplayer != null ? idplayer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Player)) {
            return false;
        }
        Player other = (Player) object;
        if ((this.idplayer == null && other.idplayer != null) || (this.idplayer != null && !this.idplayer.equals(other.idplayer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entiteti.Player[ idplayer=" + idplayer + " ]";
    }
    
}
