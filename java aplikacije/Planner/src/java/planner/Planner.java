package planner;

import entiteti.Mesto;
import entiteti.Planovi;
import entiteti.User;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Planner {

    private static final String AUDIO_PATH = "C:\\Users\\admin\\Desktop\\AlarmService\\";
    private static final String AUDIO_FORMAT = ".wav";
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("PlannerPU");
    private final EntityManager em = emf.createEntityManager();

    public String deletePlan(int id, int userId) {
        try {
            User user = em.find(User.class, userId);
            Planovi plan = em.find(Planovi.class, id);
            if (plan == null) {
                return "Plan nije validan";
            }
            System.out.println("brisanje obaveze " + id);
            if (plan.getUser() == user) {
                em.getTransaction().begin();
                em.remove(plan);
                em.getTransaction().commit();
                System.out.println("brisanje plana " + id);
                return "Obrisan plan " + id;
            } else {
                return "Nije moguce obrisati plan jer ne pripada trenutnom korisniku";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Doslo je do greske prilikom briasanja plana " + e.getMessage();
        }
    }

    public String getAllPlans(int userId) {
        try {
            User user = em.find(User.class, userId);
            List<Planovi> planovi = em.createQuery("SELECT p FROM Planovi p WHERE p.user.iduser=:idUser", Planovi.class).setParameter("idUser", user.getIduser()).getResultList();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("idPlan\topis\tlokacija\tdatum\n");
            for (Planovi plan : planovi) {
                stringBuilder.append(plan.getIdPlanovi()).append("\t").append(plan.getOpis()).append("\t").append(plan.getLokacija()).append("\t").append(plan.getDatum()).append("\n");
            }
            System.out.println("dohvatanje obaveza");
            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "Doslo je do greske prilikom dohvatanja svih planova " + e.getMessage();
        }
    }

    public String insertPlan(String opis, String lokacija, Date date, long trajanje, int userId) {
        try {
            if (date.getTime() < (new Date(System.currentTimeMillis())).getTime()) {
                return "datum ne sme biti u proslosti";
            }
            User user = em.find(User.class, userId);
            Planovi plan = new Planovi(null, opis.replace("+", " "), date, trajanje);
            List<Mesto> mesta = em.createNamedQuery("Mesto.findByLokacija", Mesto.class).setParameter("lokacija", lokacija.replace("+", " ")).getResultList();
            if (mesta == null || mesta.isEmpty()) {
                return "Uneta lokacija nije validna";
            }
            plan.setLokacija(mesta.get(0));
            plan.setUser(user);
            if (proveriJelMozeStici(plan, user)) {
                em.getTransaction().begin();
                em.persist(plan);
                em.getTransaction().commit();
                System.out.println("unos obaveze " + opis.replace("+", " ") + " " + lokacija.replace("+", " ") + " " + date + " " + user.getUsername());
                return "unos obaveze " + opis.replace("+", " ") + " " + lokacija.replace("+", " ") + " " + date + " " + user.getUsername();
            } else {
                return "nije moguce stici na ovu obavezu - nece biti uneta";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Doslo je do greske prilikom kreiranja plana " + e.getMessage();
        }
    }

    public String modifyPlan(int id, String opis, String lokacija, Date date, long trajanje, int userId) {
        try {
            User user = em.find(User.class, userId);
            Planovi plan = em.find(Planovi.class, id);
            if (plan == null) {
                return "plan ne postoji";
            }
            plan.setDatum(date);
            List<Mesto> mesta = em.createNamedQuery("Mesto.findByLokacija", Mesto.class).setParameter("lokacija", lokacija.replace("+", " ")).getResultList();
            if (mesta == null || mesta.isEmpty()) {
                return "Uneta lokacija nije validna";
            }
            plan.setLokacija(mesta.get(0));
            plan.setOpis(opis.replace("+", " "));
            plan.setUser(user);
            plan.setTrajanje(trajanje);
            if (proveriJelMozeStici(plan, user)) {
                em.getTransaction().begin();
                em.persist(plan);
                em.getTransaction().commit();
                System.out.println("izmena obaveze " + id + " " + opis.replace("+", " ") + " " + lokacija.replace("+", " ") + " " + date + " " + user.getUsername());
                return "izmena obaveze " + id + " " + opis.replace("+", " ") + " " + lokacija.replace("+", " ") + " " + date + " " + user.getUsername();
            } else {
                return "nije moguce stici na ovu obavezu - nece biti izmenjena";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Doslo je do greske prilikom briasanja plana " + e.getMessage();
        }
    }

    private boolean proveriJelMozeStici(Planovi plan, User user) {
        return vratiDistancu(plan, user) > -1;
    }

    private long vratiDistancu(Planovi plan, User user) {
        boolean flagPre = false;
        boolean flagPosle = false;
        double walking = -1;
        List<Planovi> brisiPre = new ArrayList<>();
        List<Planovi> brisiPosle = new ArrayList<>();
        List<Planovi> planoviPosle = em.createQuery("SELECT p FROM Planovi p WHERE p.user.iduser = :idUser ORDER BY p.datum", Planovi.class)
                .setParameter("idUser", user.getIduser()).getResultList();
        List<Planovi> planoviPre = em.createQuery("SELECT p FROM Planovi p WHERE p.user.iduser = :idUser ORDER BY p.datum DESC", Planovi.class)
                .setParameter("idUser", user.getIduser()).getResultList();
        planoviPosle.remove(plan);
        planoviPre.remove(plan);
        for (Planovi planPre : planoviPre) {
            if (planPre.getDatum().getTime() > plan.getDatum().getTime()) {
                brisiPre.add(planPre);
            } else {
                break;
            }
        }
        for (Planovi planPosle : planoviPosle) {
            if (planPosle.getDatum().getTime() < plan.getDatum().getTime()) {
                brisiPosle.add(planPosle);
            } else {
                break;
            }
        }
        for (Planovi temp : brisiPre) {
            planoviPre.remove(temp);
        }
        for (Planovi temp : brisiPosle) {
            planoviPosle.remove(temp);
        }

        Mesto mestoPlan = plan.getLokacija();
        if (planoviPre == null || planoviPre.isEmpty()) {//pre
            Mesto mestoPre = em.createNamedQuery("Mesto.findByLokacija", Mesto.class).setParameter("lokacija", "Beograd").getSingleResult();
            double distance = Math.sqrt(Math.pow(mestoPlan.getX() - mestoPre.getX(), 2) + Math.pow(mestoPlan.getY() - mestoPre.getY(), 2));
            if (distance * 900 * 1000 + (new Date(System.currentTimeMillis())).getTime() < plan.getDatum().getTime()) {
                flagPre = true;
                walking = distance;
            }
        } else {
            Mesto mestoPre = planoviPre.get(0).getLokacija();
            double distance = Math.sqrt(Math.pow(mestoPlan.getX() - mestoPre.getX(), 2) + Math.pow(mestoPlan.getY() - mestoPre.getY(), 2));
            if (distance * 900 * 1000 + planoviPre.get(0).getDatum().getTime() + planoviPre.get(0).getTrajanje() * 1000 < plan.getDatum().getTime()) {
                flagPre = true;
                walking = distance;
            }
        }

        if (planoviPosle != null && !planoviPosle.isEmpty()) {
            Mesto mestoPosle = planoviPosle.get(0).getLokacija();
            double distance = Math.sqrt(Math.pow(mestoPlan.getX() - mestoPosle.getX(), 2) + Math.pow(mestoPlan.getY() - mestoPosle.getY(), 2));
            if (distance * 900 * 1000 + plan.getDatum().getTime() + plan.getTrajanje() * 1000 < planoviPosle.get(0).getDatum().getTime()) {
                flagPosle = true;
            }
        }

        if (flagPre && flagPosle) {
            return (long) walking;
        } else {
            return -1;
        }
    }

    private long dohvatiDistancu(Planovi plan, User user) {
        List<Planovi> brisiPre = new ArrayList<>();
        List<Planovi> planoviPre = em.createQuery("SELECT p FROM Planovi p WHERE p.user.iduser = :idUser ORDER BY p.datum DESC", Planovi.class)
                .setParameter("idUser", user.getIduser()).getResultList();
        planoviPre.remove(plan);
        for (Planovi planPre : planoviPre) {
            if (planPre.getDatum().getTime() > plan.getDatum().getTime()) {
                brisiPre.add(planPre);
            }
        }
        for (Planovi temp : brisiPre) {
            planoviPre.remove(temp);
        }
        Mesto mestoPlan = plan.getLokacija();
        if (planoviPre == null || planoviPre.isEmpty()) {//pre
            Mesto mestoPre = em.createNamedQuery("Mesto.findByLokacija", Mesto.class).setParameter("lokacija", "Beograd").getSingleResult();
            return (long) Math.sqrt(Math.pow(mestoPlan.getX() - mestoPre.getX(), 2) + Math.pow(mestoPlan.getY() - mestoPre.getY(), 2)) * 900 * 1000;

        } else {
            Mesto mestoPre = planoviPre.get(0).getLokacija();
            return (long) Math.sqrt(Math.pow(mestoPlan.getX() - mestoPre.getX(), 2) + Math.pow(mestoPlan.getY() - mestoPre.getY(), 2)) * 900 * 1000;
        }
    }

    public String dodajObavestenjeZaObavezu(int userId, int id) {
        try {
            User user = em.find(User.class, userId);
            Planovi plan = em.find(Planovi.class, id);
            if (plan == null) {
                return "plan ne postoji";

            }
            if (plan.getDatum().getTime() > new Date(System.currentTimeMillis()).getTime()) {
                entiteti.Alarm a = em.createQuery("SELECT a FROM Alarm a WHERE a.user.iduser = :id", entiteti.Alarm.class).setParameter("id", user.getIduser()).getSingleResult();
                Date date = new Date(plan.getDatum().getTime() - dohvatiDistancu(plan, user));
                Timer timer = new Timer(true);
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            System.out.println("Alarm!!!" + user.getUsername());
                            AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File(AUDIO_PATH + a.getSong() + AUDIO_FORMAT));
                            Clip clip = AudioSystem.getClip();
                            clip.open(audioIn);
                            clip.start();
                            Thread.sleep(10000);
                            clip.stop();
                        } catch (Exception e) {
                            System.out.println("greska u obavestenju");
                            e.printStackTrace();
                        }
                    }
                }, date);
                System.out.println("navijeno obavestenje u " + date);
                return "navijeno obavestenje u " + date;
            } else {
                return "nije moguce staviti da alarm obavestenja bude u proslosti";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Doslo je do greske prilikom dodavanja obavestenja plana " + e.getMessage();
        }
    }
}
