package planner;

import java.util.Date;
import java.util.StringTokenizer;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import javax.jms.Topic;

public class Main {

    @Resource(lookup = "PlannerTopic")
    static Topic topic;
    @Resource(lookup = "UserServiceTopic")
    static Topic userTopic;
    @Resource(lookup = "jms/__defaultConnectionFactory")
    static ConnectionFactory connectionFactory;

    private static final String PLANNER_SERVICE = "Planner";
    private static final String TIP_PROPERTY = "Tip";
    private static final String OBAVEZA_PROPERTY = "OPIS_LOKACIJA";
    private static final String ID_PROPERTY = "ID_Property";
    private static final String USER_PROPERTY = "User";
    private static final String TRAJANJE = "Trajanje";

    public static void main(String[] args) {
        JMSContext context = connectionFactory.createContext();
        JMSProducer producer = context.createProducer();
        JMSConsumer consumer = context.createSharedDurableConsumer(topic, PLANNER_SERVICE);
        Planner planner = new Planner();

        while (true) {
            try {
                Message message = consumer.receive();
                int tip = message.getShortProperty(TIP_PROPERTY);
                if (message instanceof ObjectMessage) {
                    switch (tip) {
                        case 1: {
                            //obrisi
                            TextMessage producerMessage = context.createTextMessage();
                            producerMessage.setText(planner.deletePlan((int) ((ObjectMessage) message).getObject(), message.getIntProperty(USER_PROPERTY)));
                            producer.send(userTopic, producerMessage);
                            break;
                        }
                        case 2: {
                            //dohvati sve
                            TextMessage producerMessage = context.createTextMessage();
                            producerMessage.setText(planner.getAllPlans(message.getIntProperty(USER_PROPERTY)));
                            producer.send(userTopic, producerMessage);
                            break;
                        }
                        case 0: {
                            //napravi obavezu
                            StringTokenizer stringTokenizer = new StringTokenizer(message.getStringProperty(OBAVEZA_PROPERTY), "#");
                            TextMessage producerMessage = context.createTextMessage();
                            producerMessage.setText(planner.insertPlan(stringTokenizer.nextToken(), stringTokenizer.nextToken(), (Date) ((ObjectMessage) message).getObject(),
                                    message.getLongProperty(TRAJANJE), message.getIntProperty(USER_PROPERTY)));
                            producer.send(userTopic, producerMessage);
                            break;
                        }
                        case 3: {
                            //promeni obavezu
                            StringTokenizer stringTokenizer = new StringTokenizer(message.getStringProperty(OBAVEZA_PROPERTY), "#");
                            TextMessage producerMessage = context.createTextMessage();
                            producerMessage.setText(planner.modifyPlan(Integer.valueOf(stringTokenizer.nextToken()), stringTokenizer.nextToken(), stringTokenizer.nextToken(),
                                    (Date) ((ObjectMessage) message).getObject(), message.getLongProperty(TRAJANJE), message.getIntProperty(USER_PROPERTY)));
                            producer.send(userTopic, producerMessage);
                            break;
                        }
                        case 4: {
                            //dodaj alarm
                            TextMessage producerMessage = context.createTextMessage();
                            producerMessage.setText(planner.dodajObavestenjeZaObavezu(message.getIntProperty(USER_PROPERTY), (int) message.getLongProperty(ID_PROPERTY)));
                            producer.send(userTopic, producerMessage);
                            break;
                        }
                        default:
                            break;
                    }
                }
            } catch (JMSException ex) {
                try {
                    ex.printStackTrace();
                    TextMessage producerMessage = context.createTextMessage();
                    producerMessage.setText("ERROR!!!!!\n" + ex.getMessage());
                    producer.send(userTopic, producerMessage);
                } catch (JMSException ex1) {
                    ex1.printStackTrace();
                }
            }
        }
    }

}
