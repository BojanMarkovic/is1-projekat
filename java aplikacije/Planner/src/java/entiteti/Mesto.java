package entiteti;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "mesto")
@NamedQueries({
    @NamedQuery(name = "Mesto.findAll", query = "SELECT m FROM Mesto m"),
    @NamedQuery(name = "Mesto.findByIdmesto", query = "SELECT m FROM Mesto m WHERE m.idmesto = :idmesto"),
    @NamedQuery(name = "Mesto.findByLokacija", query = "SELECT m FROM Mesto m WHERE m.lokacija = :lokacija"),
    @NamedQuery(name = "Mesto.findByX", query = "SELECT m FROM Mesto m WHERE m.x = :x"),
    @NamedQuery(name = "Mesto.findByY", query = "SELECT m FROM Mesto m WHERE m.y = :y")})
public class Mesto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idmesto")
    private Integer idmesto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "lokacija")
    private String lokacija;
    @Basic(optional = false)
    @NotNull
    @Column(name = "x")
    private int x;
    @Basic(optional = false)
    @NotNull
    @Column(name = "y")
    private int y;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lokacija")
    private List<Planovi> planoviList;

    public Mesto() {
    }

    public Mesto(Integer idmesto) {
        this.idmesto = idmesto;
    }

    public Mesto(Integer idmesto, String lokacija, int x, int y) {
        this.idmesto = idmesto;
        this.lokacija = lokacija;
        this.x = x;
        this.y = y;
    }

    public Integer getIdmesto() {
        return idmesto;
    }

    public void setIdmesto(Integer idmesto) {
        this.idmesto = idmesto;
    }

    public String getLokacija() {
        return lokacija;
    }

    public void setLokacija(String lokacija) {
        this.lokacija = lokacija;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public List<Planovi> getPlanoviList() {
        return planoviList;
    }

    public void setPlanoviList(List<Planovi> planoviList) {
        this.planoviList = planoviList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmesto != null ? idmesto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mesto)) {
            return false;
        }
        Mesto other = (Mesto) object;
        if ((this.idmesto == null && other.idmesto != null) || (this.idmesto != null && !this.idmesto.equals(other.idmesto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entiteti.Mesto[ idmesto=" + idmesto + " ]";
    }
    
}
