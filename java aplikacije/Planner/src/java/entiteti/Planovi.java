package entiteti;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "planovi")
@NamedQueries({
    @NamedQuery(name = "Planovi.findAll", query = "SELECT p FROM Planovi p"),
    @NamedQuery(name = "Planovi.findByIdPlanovi", query = "SELECT p FROM Planovi p WHERE p.idPlanovi = :idPlanovi"),
    @NamedQuery(name = "Planovi.findByOpis", query = "SELECT p FROM Planovi p WHERE p.opis = :opis"),
    @NamedQuery(name = "Planovi.findByDatum", query = "SELECT p FROM Planovi p WHERE p.datum = :datum"),
    @NamedQuery(name = "Planovi.findByTrajanje", query = "SELECT p FROM Planovi p WHERE p.trajanje = :trajanje")})
public class Planovi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPlanovi")
    private Integer idPlanovi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "opis")
    private String opis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Datum")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "trajanje")
    private long trajanje;
    @JoinColumn(name = "Lokacija", referencedColumnName = "lokacija")
    @ManyToOne(optional = false)
    private Mesto lokacija;
    @JoinColumn(name = "user", referencedColumnName = "iduser")
    @ManyToOne(optional = false)
    private User user;

    public Planovi() {
    }

    public Planovi(Integer idPlanovi) {
        this.idPlanovi = idPlanovi;
    }

    public Planovi(Integer idPlanovi, String opis, Date datum, long trajanje) {
        this.idPlanovi = idPlanovi;
        this.opis = opis;
        this.datum = datum;
        this.trajanje = trajanje;
    }

    public Integer getIdPlanovi() {
        return idPlanovi;
    }

    public void setIdPlanovi(Integer idPlanovi) {
        this.idPlanovi = idPlanovi;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public long getTrajanje() {
        return trajanje;
    }

    public void setTrajanje(long trajanje) {
        this.trajanje = trajanje;
    }

    public Mesto getLokacija() {
        return lokacija;
    }

    public void setLokacija(Mesto lokacija) {
        this.lokacija = lokacija;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPlanovi != null ? idPlanovi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Planovi)) {
            return false;
        }
        Planovi other = (Planovi) object;
        if ((this.idPlanovi == null && other.idPlanovi != null) || (this.idPlanovi != null && !this.idPlanovi.equals(other.idPlanovi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entiteti.Planovi[ idPlanovi=" + idPlanovi + " ]";
    }
    
}
